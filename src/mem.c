#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static struct region alloc_region(void const *addr, size_t capacity) {
    size_t length = region_actual_size(size_from_capacity((block_capacity){.bytes = capacity}).bytes);
    void* compare = map_pages(addr, length, MAP_FIXED_NOREPLACE);
    if (compare == MAP_FAILED) {
        compare = map_pages(addr, length, 0);
        if (compare == MAP_FAILED) return REGION_INVALID;
    }
    block_init(compare, (block_size) {length}, NULL);
    return (struct region) { .addr = compare, .size = length, .extends = compare == addr };
}

static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd );

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;
    return region.addr;
}

void heap_term() {
    struct block_header* current_block = (struct block_header*) HEAP_START;
    void* region_start = HEAP_START;

    while (current_block) {
        struct block_header* next_block = current_block->next;

        if (!next_block || !blocks_continuous(current_block, next_block)) {
            size_t region_size = (size_t)((char*)current_block - (char*)region_start) + size_from_capacity(current_block->capacity).bytes;
            if (munmap(region_start, region_size) == -1) exit(1);
            region_start = next_block;
        }
        current_block = next_block;
    }
}


#define BLOCK_MIN_CAPACITY 24
#define ALIGNMENT 16

static size_t align_size(size_t size) {
    return (size + ALIGNMENT - 1) / ALIGNMENT * ALIGNMENT;
}

static bool block_splittable(struct block_header* restrict block, size_t query) {
    size_t aligned_query = align_size(query + offsetof(struct block_header, contents));
    size_t total_size_required = aligned_query + BLOCK_MIN_CAPACITY;
    return block->is_free && block->capacity.bytes >= total_size_required;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block || !block_splittable(block, query)) return false;

    block_capacity capacity = {size_max(query, BLOCK_MIN_CAPACITY)};
    struct block_header *new_block_addr = (struct block_header *) (block->contents + capacity.bytes);
    block_init(new_block_addr, (block_size) {block->capacity.bytes - capacity.bytes}, block->next);
    block_init(block, size_from_capacity(capacity), new_block_addr);
    return true;
}


static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next(struct block_header* block) {
    if (block == NULL || block->next == NULL) return false;
    struct block_header* next_block = block->next;
    if (mergeable(block, next_block)) {
        block->capacity.bytes += next_block->capacity.bytes + offsetof(struct block_header, contents);
        block->next = next_block->next;
        return true;
    }
    return false;
}

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header *block, size_t size) {
    if (!block) return (struct block_search_result){ .type = BSR_CORRUPTED, .block = NULL };
    struct block_header *current = block;
    while (current) {
        while (try_merge_with_next(current));
        if (current->is_free && block_is_big_enough(size, current)) return (struct block_search_result){ .type = BSR_FOUND_GOOD_BLOCK, .block = current };
        if (!current->next) return (struct block_search_result){ .type = BSR_REACHED_END_NOT_FOUND, .block = current };
        current = current->next;
    }
    return (struct block_search_result){ .type = BSR_CORRUPTED, .block = NULL };
}

static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    query = align_size(query);
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK && split_if_too_big(result.block, query)) result.block->is_free = false;
    return result;
}

static struct block_header* grow_heap(struct block_header* last, size_t query) {
    if (last == NULL) return NULL;
    query = align_size(query);
    struct region region = alloc_region(block_after(last), query);
    if (region_is_invalid(&region)) return NULL;
    last->next = region.addr;
    return (region.extends && try_merge_with_next(last)) ? last : (struct block_header*)region.addr;
}

static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = align_size(query);
    if (heap_start == NULL) {
        struct region region = alloc_region(heap_start, query);
        if ( region_is_invalid(&region) ) return NULL;
        heap_start = region.addr;
    }
    struct block_header *block = heap_start;
    while (true) {
        struct block_search_result result = try_memalloc_existing(query, block);
        if (result.type == BSR_FOUND_GOOD_BLOCK) {
            if (split_if_too_big(result.block, query)) return result.block;
            result.block->is_free = false;
            return result.block;
        }
        if (result.type == BSR_REACHED_END_NOT_FOUND && !grow_heap(result.block, query)) return NULL;
        if (result.type == BSR_CORRUPTED) return NULL;
    }
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (header && try_merge_with_next(header)) {
        header = header->next;
    }
}
